> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Install JSP Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

> #### Git commands w/short descriptions:

1. git init - creates an empty Git repository - basically a git directory
2. git status - displays paths that have differences between the index file and the current HEAD commit
3. git add - updates the index using the current content found in the working tree 
4. git commit - records changes to the repository
5. git push - update remote refs along with associated objects 
6. git pull - fetch from and intergrate with another repository or a local branch
7. git merge - join two or more development histories together

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![Android Studio Installation Screenshot](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/yourmobilegeek/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/yourmobilegeek/myteamquotes "My Team Quotes Tutorial")