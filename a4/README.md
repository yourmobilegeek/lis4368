> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Assignment 4 Requirements:

*Deliverables:*

1. Failed Validation
2. Passed Validation

#### README.md file should include the following items:

#### Assignment Screenshots: 

|*Failed Validaton:*|Passed Validation:|
|-----------------|------------------|------------------|
|![Splash Page Screenshot](img/failed.png)|![Non - Playing Music Screenshot](img/pass.png)
