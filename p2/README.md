> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Project 2 Requirements:

#### README.md file should include the following items:

1. Bitbucket repo link1. 
2. Screenshot of User Form Entry; 
3. Screenshot of passed validation;
4. Screenshot of displayed data;

#### Assignment Screenshots:

|*user form entry:*|*passed validation:*|display data:|
|-----------------|------------------|------------------|
![User Form Entry Screenshot](img/user.png)|![Passed Validation Screenshot](img/passed.png)|![Display Data Screenshot](img/data.png)|
