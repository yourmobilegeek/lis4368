> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Assignment 2 Requirements:

*Two Parts:*

1. Assessmnet Links
2. Screenshot of query results

#### README.md file should include the following items:

#### Assessment Links:

*Link: localhost:999/hello*
[Link: localhost:999/hello](http://localhost:9999/hello)

*Link: HelloHome, index.html*
[http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)

*Link: querybook*
[Link: http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

*Link: AnotherHelloServlet*
[Link: http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)


#### Assignment Screenshots:

*Screenshot of Homepage*:

![Homepage Screenshot](img/home.png)

*Screenshot of SayHi*:

![SayHi Screenshot](img/sayhi.png)

*Screenshot of QueryBook*:

![QueryResults Screenshot](img/query.png)
