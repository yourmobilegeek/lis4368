> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis 

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat 
    - Provide screenshots and installations  
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Installing and using MySQL
    - Creating a new user on MySQL
    - Grant permissions to users 
    - Creating tables, and adding information in MySQL 
    - Use @WebServlet (annotation) declares servlet configuration
    - Create hello page, query page and query results

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD) 
    - Include data (at least 10 records each table)
    - Provide Bitbucket read-only access to repo (Language SQL) must include README.md using Markdown syntx and include links to all of the following files (from README.md)
    - Blackboard Links: Bitbucket Repo

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create P1 index page
    - Validate user input to each specific type
    - Give error messages when a user inputs wrong values
    - Show user when input has been properly inputed

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create index.jsp file 
    - Create a server side validation for 'store' database 
    - Upload database with proper edits

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Compile the following class and servlet files 
    - Include server-side validation from A4
    - Display screenshots of pre-, post-valid user form entry, as well as MYSQL customer table 
    - Git push to *all* lis4368 files

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Compile the following class and servlet files 
    - Include server-side validation from A4 
    - Display screenshots of pre-, post-valid user form entry, as well as MYSQL customer table 
    - Git push to *all* lis4368 files




