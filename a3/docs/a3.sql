-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema amd14b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `amd14b` ;

-- -----------------------------------------------------
-- Schema amd14b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `amd14b` DEFAULT CHARACTER SET latin1 ;
SHOW WARNINGS;
USE `amd14b` ;

-- -----------------------------------------------------
-- Table `amd14b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amd14b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `amd14b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `amd14b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amd14b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `amd14b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `amd14b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `amd14b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `amd14b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m','f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'pet store\'s cost',
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL COMMENT 'what pet sold for',
  `pet_age` TINYINT UNSIGNED NOT NULL COMMENT 'age is in weeks, divide by 52 for yrs.',
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y','n') NOT NULL,
  `pet_neuter` ENUM('y','n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_store1_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_store1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `amd14b`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `amd14b`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `amd14b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `amd14b`;
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Claws and Paws', '123 Main St.', 'L.A.', 'CA', 090195341, 8185551234, 'clawspaws@aol.com', 'http://www.clawspaws.com', 50000, 'notes1');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '1435 Chase Dr.', 'Chicago', 'IL', 618959031, 8471234567, 'furryfriends@hotmail.com', 'http://www.furryfriends.com', 65000, 'notes2');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Partners', '811 4th St.', 'NY', 'NY', 001256728, 2129876543, 'petpartners@yahoo.com', 'http://www.petpartners.com', 87000, 'notes3');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Palace', '18903 Tennessee St.', 'Tallahassee', 'FL', 000036790, 8504445123, 'petpalace@gmail.com', 'http://www.petpalace.com', 34000, 'notes4');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Purrs and Grrs', '751 Lois Lane', 'Phoenix', 'AZ', 852122156, 4809130015, 'pursandgrrs@msn.com', 'http://www.purrsandgrrs.com', 97500, 'notes5');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pedigree Bob', '751 Lois Lane', 'Miami ', 'FL', 000036790, 8471234567, 'PedigreeBob@yahoo.com', 'http://www.pedigreebob.com', 35000, 'notes8');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Wildwood Exotic Pets', '751 Lois Lane', 'Orlando ', 'FL', 852122156, 4809130015, 'furrygeorge@gmail.com', 'http://www.furrygeorge.com', 87000, 'notes6');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Fiesta', '765 Valley Road', 'Brooklyn', 'NY', 000036790, 8471234567, 'clawspal@aol.com', 'http://www.clawspal@aol.com', 55000, 'notes10');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Slugs, Snails and Puppydog Tails', '123 Lawrence Street', 'Tampa', 'FL', 000036790, 8504445123, 'clawspalny@aol.com', 'http://www.clawspalny@ao.com', 86000, 'notes7');
INSERT INTO `amd14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Out of the Water Exotic Pets', '456 Virginia Avenue', 'Tampa', 'FL', 000036790, 8471234567, 'petpalacefl@gmail.com', 'http://www.petpalacefl.com', 60000, 'notes9');

COMMIT;


-- -----------------------------------------------------
-- Data for table `amd14b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `amd14b`;
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Billy', 'Bob', '502 Shady Dr.', 'Minneapolis', 'MN', 004011238, 6124567890, 'bbob@aol.com', 139.52, 987.61, 'notes1');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bobby', 'Sue', '19567 Rutherford Cir.', 'Charleston', 'WV', 058349012, 3016731278, 'sbobby@hotmail.com', 49.87, 1356.89, 'notes2');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Stevie', 'Ray', '8624 Baldwin Dr.', 'Panama City Beach', 'FL', 324059134, 8509980123, 'sray@fsu.edu', 9.34, 567.63, 'notes3');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Felicia', 'Fuller', '7831 Tempest Ln.', 'Sturgis', 'SD', 598431021, 5052875412, 'ffuller@msn.com', 0.0, 784.29, 'notes4');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Victoria', 'Valis', '63178 Northshore Dr.', 'Laramie', 'WY', 000457612, 7071236547, 'vvalis@yahoo.com', 1189.32, 19456.78, 'notes5');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Matthew', 'Davis', '7864 Lawrence Street', 'Tampa ', 'FL', 000457616, 5052875321, 'matthew@aol.com', 10.34, 568.63, 'notes6');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Josh', 'Langford', '6745 Virginia Avenue', 'Orlando ', 'FL', 598431567, 5052875887, 'josh@yahoo.com', 11.34, 569.63, 'notes7');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'DeAnna', 'Glover', '8967 Valley Road', 'Miami', 'FL', 324059234, 5052875256, 'dee@yahoo.com', 12.34, 570.63, 'notes8');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Justine', 'Oliver', '5638 Magnolia Avenue', 'Orlando ', 'FL', 058349223, 5052875987, 'justine@aol.com', 13.34, 571.63, 'notes9');
INSERT INTO `amd14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Erica', 'Davis', '3246 Cedar Lane', 'Miami ', 'FL', 058349876, 5052875457, 'erica@aol.com', 14.34, 572.63, 'notes10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `amd14b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `amd14b`;
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 5, 'Doberman', 'm', 350, 525, 52, 'black/tan', '2009-07-05', 'y', 'y', 'notes1');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'Chiwawa', 'f', 175, 165, 78, 'white/brown', NULL, 'n', 'n', 'notes2');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 5, 'American Bobtail', 'm', 195, 385, 104, 'black', '2011-12-24', 'y', 'y', 'notes3');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'American Longhair', 'm', 165, 275, 156, 'white', '2005-08-01', 'n', 'y', 'notes4');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'British Shorthair', 'f', 155, 145, 28, 'grey', NULL, 'y', 'n', 'notes5');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 5, 'Rose-Ringed Parakeet', 'f', 18, 32, 2, 'green/yellow', '2010-11-12', 'n', 'n', 'notes6');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 4, 'African Grey Parrot', 'm', 1295, 1800, 520, 'grey/blue', '2007-09-19', 'n', 'n', 'approx. 10 yrs old');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 1, 'Pitbull', 'm', 300, 495, 8, 'black/white', '2001-05-27', 'n', 'y', 'notes8');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 2, 'Boa Constrictor', 'f', 450, 735, 260, 'grey/green', '2006-10-31', 'n', 'n', 'approx. 5 yrs old');
INSERT INTO `amd14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'Giraffe', 'f', 1735, 2495, 208, 'yellow and black spots', NULL, 'n', 'n', 'notes10');

COMMIT;

