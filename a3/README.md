> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Assignment 3 Requirements:

*Deliverables:*

1. Screenshot A3 ERD
2. Access to A3 MWB File 
3. Access to A3 SQL File

#### README.md file should include the following items:

#### Assignment Screenshots:

*Screenshot A3 ERD Homepage*:

![A3 ERD](img/a3_erd.png " ERD based upon A3 Requirements ")

#### A3 docs: a3.mwb and a3.sql: 

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
