> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Assignment 5 Requirements:

*Deliverables:*

    - Compile the following class and servlet files 
    - Include server-side validation from A4 
    - Display screenshots of pre-, post-valid user form entry, as well as MYSQL customer table 
    - Git push to *all* lis4368 files

#### README.md file should include the following items:

#### Assignment Screenshots: 

|*Valid User Form Entry:*|*Passed Validation*|Associated Database Entry:|
|-----------------|------------------|------------------|
|![Valid User Form Entry](img/valid.png)|![Passed Validation*](img/passed.png)|![Associated Database Entry*](img/database.png)
s