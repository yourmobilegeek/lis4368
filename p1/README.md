> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Mobile Web Applications 

## Ariana M. Davis

### Project 1 Requirements:

#### README.md file should include the following items:

1. Screenshot failed & passed validation 
2. Bitbucket repo link
3. Screenshot of failed validation;
4. Screenshot of passed validation;

#### Assignment Screenshots:

|*splashpage:*|*passed validation:*|failed validation:|
|-----------------|------------------|------------------|
![Splash Page Screenshot](img/splash.png)|![ERD Screenshot](img/passed_validation.png)|![ERD Screenshot](img/failed_validation.png)|
